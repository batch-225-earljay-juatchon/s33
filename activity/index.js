// 3.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {
    console.log(json);

    // 4.
    const titles = json.map((x) => {
        return x.title
    });
    console.log(titles);
});



// 5.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {
    console.log(json);
// 6.
    console.log(`The item ${json.title} on the list has a status of ${json.completed}`);
});



// 7.
fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Created To Do List Item',
        completed: false,
        userId: 1
    })
}).then((response) => response.json())
.then((json) => console.log(json));



// 8-9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
          dateCompleted: 'Pending',
          discription: 'To update the my to do list with a different data structure',
          id: 1,
          status: 'Pending',
          title: 'Updated To Do List Item',
          userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));



// 10-11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
        completed: true,
          dateCompleted: '07/09/21',
          id: 1,
          status: 'Complete',
          title: 'delectus aut autem',
          userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));


// 12.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});